# Note that these details are compiled from a series of messages, and that this may result in some inaccuracies

# Cybersec CTF

## Prerequisites

- Able to read text files and bytes using Python, use Python libraries
- Basic knowledge of Java and C# (ifs, loops, and functions)
- NodeJS and AJAX requests
- Foundational SQL (querying a table, etc)
- Assembly (x86, ARM)
- Knowledge of low level C/C++ functions
- Knowledge of system calls ([Windows](https://docs.microsoft.com/en-us/cpp/c-runtime-library/system-calls?view=vs-2019), [Linux](http://man7.org/linux/man-pages/man2/syscalls.2.html))

Don't worry about trying to remember the syscalls as nobody does that :). It's just important so that you may recognize it when you see that call being used.

## Things To Understand

- How the majority of web applications are laid out
- How network and hardware protocols work
- How languages are compiled and run in machines
- How the frontend and backend systems handle communication between the clients and the database
- Know about the languages and frameworks used, along with the tech stack of web applications
- How *nix systems work, such as FreeBSD and Debian
- Foundational knowledge on how Windows works, such as network protocols, batch commands, and the registry
- HTTP requests, Websockets, UDP, TCP
- How languages are compiled from languages (C, C++, Rust) to a particular architecture's assembly
- How languages are compiled to bytecode (Java, C#) that gets run on a virtual machine
- RS232 and digital signals

While this may seem like a lot, there are many ways that you can get up to speed. I recommend a YouTube channel called [LiveOverflow](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w) and another one called [MalwareAnalysisForHedgehogs](https://www.youtube.com/channel/UCVFXrUwuWxNlm6UNZtBLJ-A). Furthermore, Monash students can also get all the Tech books from O'Reilly for free, and there are books for Assembly and Cyptography. These books are in digital form so you can read them anywhere you are. I will also share the other books once you guys start to understand or at least start to be aware of how everything works.

Note that you don't have to read all the chapters, just read a bit of each to know which one you should know. But feel free to ask me directly if you want to jump into it faster.

Some other books I recommend:  
![Assembly Books](Assembly.jpeg)
![Applied Cryptography: Protocols, Algorithms, and Source Code in C](Cryptography.jpeg)
